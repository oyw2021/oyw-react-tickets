import React from "react";
import { Svg, Showbox, Loader as LoaderSC } from "./Loader";
import "./loader.css";

const Loader: React.FC = () => {
  return (
    <Showbox>
      <LoaderSC>
        <Svg className="circular" viewBox="25 25 50 50">
          <circle
            className="path"
            cx="50"
            cy="50"
            r="20"
            fill="none"
            strokeWidth="4"
            strokeMiterlimit="10"
          ></circle>
        </Svg>
      </LoaderSC>
    </Showbox>
  );
};

export default Loader;
