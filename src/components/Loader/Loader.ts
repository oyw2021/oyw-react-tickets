import styled from "styled-components";

export const Showbox = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 0;
  z-index: 999;
  background-color: rgba(0, 0, 0, 0.3);
`;

export const Loader = styled.div`
  position: relative;
  margin: 0 auto;
  width: 70px;
  background: #fff;
  box-shadow: 0 4px 10px 0 rgba(0, 0, 0, 0.1);
  top: 50%;
  border-radius: 100%;
  transform: translateY(-50%);
  &:before {
    content: "";
    display: block;
    padding-top: 100%;
  }
`;

export const Icon = styled.svg.attrs({
  version: "1.1",
  xmlns: "http://www.w3.org/2000/svg",
  xmlnsXlink: "http://www.w3.org/1999/xlink",
})``;

export const Svg = styled(Icon)`
  animation: rotate 2s linear infinite;
  height: 65%;
  transform-origin: center center;
  width: 65%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
`;

export const path = styled.div`
  stroke-dasharray: 1, 200;
  stroke-dashoffset: 0;
  animation: dash 1.5s ease-in-out infinite, color 6s ease-in-out infinite;
  stroke-linecap: round;

  @keyframes rotate {
    100% {
      transform: rotate(360deg);
    }
  }

  @keyframes dash {
    0% {
      stroke-dasharray: 1, 200;
      stroke-dashoffset: 0;
    }
    50% {
      stroke-dasharray: 89, 200;
      stroke-dashoffset: -35px;
    }
    100% {
      stroke-dasharray: 89, 200;
      stroke-dashoffset: -124px;
    }
  }
  @keyframes color {
    100%,
    0% {
      stroke: #8660ce;
    }
  }
`;
