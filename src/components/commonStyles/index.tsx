import styled, { keyframes } from "styled-components";
import { toast } from "react-toastify";

export const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
`;
export const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

export const PageContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  max-width: 100vw;
  min-height: 100vh;
  overflow-y: hidden;
  overflow-x: hidden;
  &::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  &::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
`;

const gradient = keyframes`
    0% {
		background-position: 0% 50%;
	}
	50% {
		background-position: 100% 50%;
	}
	100% {
		background-position: 0% 50%;
	}
`;

export const GradientBackgroundContainer = styled.div`
  background: radial-gradient(
    ellipse farthest-corner at 30% 40%,
    #f82d79,
    #fb9855,
    #7bcabc,
    #fff
  );
  background-size: 400% 400%;
  animation: ${gradient} 15s ease infinite;
  height: 100%;
  min-height: 115vh;
  min-width: 100vw;
  -webkit-mask-image: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0),
    rgba(0, 0, 0, 1)
  );
  mask-image: linear-gradient(to top, transparent, white 15%);
  /* mask-image: -webkit-gradient(
    linear,
    left top,
    left bottom,
    from(rgba(0, 0, 0, 1)),
    to(transparent)
  ); */
  /* webkit-mask-image: -webkit-gradient(
    linear,
    left top,
    left bottom,
    from(rgba(0, 0, 0, 1)),
    to(rgba(0, 0, 0, 0))
  ); */

  background-color: white;
`;



// Glowing body button

// export const LiveStreamButton = styled.button`
//   max-width: 320px;
//   border: none;
//   background: transparent;
//   cursor: pointer !important;
//   padding: 16px 48px;
//   -webkit-box-shadow: 0 0 20px blue;
//   -moz-box-shadow: 0 0 20px blue;
//   box-shadow: 0 0 20px blue;
//   border-radius: 16px;
//   &:hover {
//     animation: ${glow} 1s ease-in infinite;
//   }
// `;

const glowing = keyframes`from {
    box-shadow: 0 0 20px #fff;
  }
  to {
    box-shadow:  0 0 75px #fff;
    transform: scale(1.05);
  }`;

export const LiveStreamButton = styled.button`
  max-width: 320px;
  border: 3px solid white;
  background: transparent;
  cursor: pointer !important;
  padding: 16px 48px;
  -webkit-animation: ${glowing} 1s ease-in-out infinite alternate;
  -moz-animation: ${glowing} 1s ease-in-out infinite alternate;
  animation: ${glowing} 1s ease-in-out infinite alternate;
  border-radius: 16px;
`;

export const WhiteCard = styled.div`
  padding: 20px;
  border-radius: 8px;
  box-shadow: 0 -12px 22px 0 rgba(82, 87, 104, 0.05),
    0 12px 22px 0 rgba(82, 87, 104, 0.1);
  background-color: #ffffff;
  flex: 1;
`;

export const LoginTextInput = styled.input`
  outline: 1px solid #182b4e;
  border: 0;
  background: transparent;
  border-radius: 4px;
  border-bottom: 1px solid white;
  /* color: white; */
  width: 100%;
  font-weight: 600;
  padding: 8px;
  font-size: 16px;
  font-family: "Outfit";
  ::placeholder {
    /* color: #ffffff; */
  }
`;

export const CountContainer = styled.div`
  border: 1px solid #182b4e;
  padding: 16px;
  border-radius: 8px;
  flex: 1;

  box-shadow: blue 0px 0px 0px 2px inset, rgb(255, 255, 255) 10px -10px 0px -3px,
    rgb(31, 193, 27) 10px -10px, rgb(255, 255, 255) 20px -20px 0px -3px,
    rgb(255, 217, 19) 20px -20px, rgb(255, 255, 255) 30px -30px 0px -3px,
    rgb(255, 156, 85) 30px -30px, rgb(255, 255, 255) 40px -40px 0px -3px,
    rgb(255, 85, 85) 40px -40px;
`;

export const CopyrightBanner = styled.div`
  background-color: #202020;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const AppContainer = styled.div`
  width: 100vw !important;
  max-width: 100vw;
  overflow-x: hidden !important;
  min-height: 100vh;
  &::-webkit-scrollbar:horizontal {
    height: 0;
    width: 0;
    display: none;
  }

  &::-webkit-scrollbar-thumb:horizontal {
    display: none;
  }
`;

export const showToastMessageSuccess = (message: string) => {
  toast.success(message, {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
  });
};

export const showToastMessageFailed = (message: string) => {
  toast.error(message, {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
  });
};

export const OutlinedButton = styled.button`
  border-radius: 4px;
  border: 1px solid #183b4e;
  width: 100%;
  max-width: 240px;
  background: none;
  padding: 8px;
  cursor: pointer;

  &:hover {
    transform: scale(1.05);
  }
`;

export const SearchBarContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 8px;
  border: 1px solid #b0bec9;
  color: #b0bec9;
  border-radius: 8px;
  flex: 0.4;
  > input {
    flex: 1;
    background-color: transparent;
    margin-left: 12px;
    outline: 0px;
    border: 0px;
    ::placeholder {
      color: #b0bec9;
    }
  }
`;
