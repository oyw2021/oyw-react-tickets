import React from "react";
import { ListTileHeaderContainer, ListTileContainer } from "./style";
import { OutfitText } from "../commonText";

import { FlexColumn } from "../commonStyles";
import { isMobile } from "react-device-detect";

interface Props {
  loading: boolean;
  columnHeaders?: { text: string; flexValue: string }[];
  columnValues: {
    content: React.ReactNode;
    flexValue: number;
  }[][];
}

const ListComponent: React.FC<Props> = ({ columnHeaders, columnValues }) => {
  return (
    <>
      {columnHeaders && (
        <ListTileHeaderContainer>
          {columnHeaders.map((header, index) => (
            <OutfitText
              style={{
                flex: header.flexValue,
                fontSize: isMobile ? "10px" : "24px",
                color: "white",
              }}
              key={index}
            >
              {header.text}
            </OutfitText>
          ))}
        </ListTileHeaderContainer>
      )}
      <FlexColumn>
        {columnValues.map((row, index) => {
          return (
            <ListTileContainer key={index}>
              {row.map((value, index) => {
                return (
                  <div key={index} style={{ flex: value.flexValue }}>
                    {value.content}
                  </div>
                );
              })}
            </ListTileContainer>
          );
        })}
      </FlexColumn>
    </>
  );
};

export default ListComponent;
