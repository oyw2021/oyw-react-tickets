import styled from "styled-components";

export const ListTileHeaderContainer = styled.div`
  display: flex;
  background: #090533;
  align-items: center;
  height: 38px;
  padding: 24px 10px;
  border-radius: 8px;
  font-size: 15px;
  font-weight: 600;
  color: #98989b;
`;

export const ListTileContainer = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  border-radius: 4px;
  border-bottom: 1px solid rgba(176, 190, 201, 0.5);
  border-radius: 8px;
  padding: 16px 10px;
`;

export const LoadingShimmer = styled.div(
  (props: { height?: string; width?: string }) => `
    width: 75%;
    background-color: #e8e8e8;
    height: 22px;
    background-color: #eee;
    border-radius: 7px;
    @keyframes placeHolderShimmer {
      0% {
        background-position: -468px 0;
      }
      100% {
        background-position: 468px 0;
      }
    }
  `
);

export const AnimatedBackground = styled.div(
  (props) => `
    animation-duration: 1.25s;
    animation-fill-mode: forwards;
    animation-iteration-count: infinite;
    animation-name: placeHolderShimmer;
    animation-timing-function: linear;
    background: darkgray;
    background: linear-gradient(to right, #eeeeee 10%, #dddddd 18%, #eeeeee 33%);
    background-size: 800px 104px;
    height: inherit;
    position: relative;
    border-radius: 7px;
  `
);
