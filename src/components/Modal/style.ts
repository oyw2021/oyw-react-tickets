import styled from "styled-components";

export const ModalContainer = styled.div`
  position: fixed;
  left: 0px;
  right: 0px;
  top: 0px;
  bottom: 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 9;
`;

export const ModalContent = styled.div`
  /* min-width: 480px; */
  max-height: 700px;
  padding: 15px;
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  box-shadow: 0 2px 11px 0 rgba(155, 188, 227, 0.29);
  background-color: #ffffff;
  ::-webkit-scrollbar {
    width: 0px;
    background: transparent; /* make scrollbar transparent */
  }
`;
