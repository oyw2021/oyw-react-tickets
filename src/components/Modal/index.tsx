import React, { useEffect } from "react";
import { ModalContainer, ModalContent } from "./style";

interface Props {
  width?: string;
  onClose?: () => void;
}

const Modal: React.FC<Props> = ({ children, width, onClose }) => {
  useEffect(() => {
    const close = (e: any) => {
      if (e.key === "Escape") {
        if (onClose) {
          onClose();
        }
      }
    };

    window.addEventListener("keydown", close);
    return () => window.removeEventListener("keydown", close);
  }, []);
  return (
    <ModalContainer style={{ maxHeight: "100vh" }}>
      <ModalContent
        style={{ width: width ? width : "480px", overflowY: "scroll" }}
      >
        {children}
      </ModalContent>
    </ModalContainer>
  );
};

export default Modal;
