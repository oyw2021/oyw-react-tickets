import { isMobile } from "react-device-detect";
import { FlexColumn, LoginTextInput, PageContainer } from "../../components/commonStyles";
import OYW21 from "../../images/icon-192x192.png";
import { useParams } from "react-router";
import QRCode from "react-qr-code";
import { NavHeaderText, OutfitText } from "../../components/commonText";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { useEffect, useState } from "react";
import { getPassDetails } from "../../store/passSlice";
import ListComponent from "../../components/ListComponent";
import { Link } from "react-router-dom";
import axios from "axios";
import { API_SERVER_URL } from "../../utils/constants";


const Passes = () => {
  
  const [email, setEmail] = useState<string>("");

  let response: { [key: string]: any } = axios.post(API_SERVER_URL + "/tickets/passes/", {"email": email});
  if (response.data.result) {
    console.log(response)
  }

  return (
    <PageContainer style={{ padding: isMobile ? "16px" : "48px" }}>
      <FlexColumn style={{ alignItems: "center", gap: "24px" }}>
        <img src={OYW21} alt="" style={{ height: "120px", width: "120px" }} />
          <>
          <OutfitText>Send Pass Email</OutfitText>
          
          <LoginTextInput
                placeholder="Email Address"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
              />
          </>
          </FlexColumn></PageContainer>
  );
};

export default Passes;
