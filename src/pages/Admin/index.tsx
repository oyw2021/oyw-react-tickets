import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import {
  FlexColumn,
  FlexRow,
  LoginTextInput,
  PageContainer,
} from "../../components/commonStyles";
import {
  disableAdmin,
  enableAdmin,
  fetchAdmins,
  registerAdmin,
  toggleAddAdminModalOpen,
} from "../../store/adminSlice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import Loader from "../../components/Loader";
import ListComponent from "../../components/ListComponent";
import { ErrorText, OutfitText } from "../../components/commonText";
import moment from "moment";
import { Switch } from "antd";
import "antd/dist/antd.css";
import cookie from "react-cookies";
import { Navigate } from "react-router";
import Modal from "../../components/Modal";
import Close from "../../images/Close.png";
import { USER_ROLES } from "../../utils/constants";
import Joi from "joi";
import Navbar from "../../components/TicketsNavbar";

const AdminValidation = Joi.object()
  .keys({
    name: Joi.string().min(3).required(),
    phone: Joi.string()
      .length(10)
      .pattern(/^[0-9]+$/)
      .required(),
    password: Joi.string().min(8),
    role: Joi.string().length(2).required(),
  })
  .required();

const Admin = () => {
  const user_role: string = cookie.load("user_role");
  const token: string = cookie.load("token");

  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(fetchAdmins());
  }, []);

  const { admins, loading, addAdminModalOpen } = useAppSelector(
    (state) => state.entities.admin
  );
  const [error, setError] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [role, setRole] = useState<string>("");
  // const [isNavOpen, setIsNavOpen] = useState<boolean>(false);

  const onSubmit = () => {
    let validate = AdminValidation.validate({ phone, name, password, role });
    if (!validate.error) {
      setError("");
      dispatch(registerAdmin({ name, phone, password, role }));
    } else {
      setError(validate.error.message);
    }
  };

  if (user_role === undefined) return <Navigate to="/tickets/signin" />;
  return loading ? (
    <Loader />
  ) : (
    <>
      <Navbar />
      <PageContainer style={{ padding: isMobile ? "16px" : "48px" }}>
        {addAdminModalOpen && (
          <Modal width={isMobile ? "90%" : "480px"}>
            <FlexColumn style={{ gap: "16px" }}>
              <FlexRow style={{ justifyContent: "flex-end" }}>
                <img
                  src={Close}
                  alt=""
                  style={{ width: "24px", height: "24px" }}
                  onClick={() => dispatch(toggleAddAdminModalOpen(false))}
                />
              </FlexRow>
              <FlexColumn
                style={{
                  justifyContent: "flex-end",
                  height: "100%",
                  gap: "16px",
                }}
              >
                <LoginTextInput
                  placeholder="Name"
                  onChange={(e) => setName(e.target.value)}
                  value={name}
                />
                <LoginTextInput
                  placeholder="Phone"
                  onChange={(e) => setPhone(e.target.value)}
                  value={phone}
                />
                <LoginTextInput
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                  type="password"
                />
                <FlexRow style={{ gap: "8px", justifyContent: "space-evenly" }}>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: "1px solid #182b4e",
                      flex: 1,
                      width: "32px",
                      textAlign: "center",
                      color: role === USER_ROLES.RU ? "white" : "#182b4e",
                      backgroundColor:
                        role === USER_ROLES.RU ? "#182b4e" : "white",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setRole(USER_ROLES.RU);
                    }}
                  >
                    {USER_ROLES.RU}
                  </div>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: "1px solid #182b4e",
                      flex: 1,
                      width: "32px",
                      textAlign: "center",
                      color: role === USER_ROLES.RH ? "white" : "#182b4e",
                      backgroundColor:
                        role === USER_ROLES.RH ? "#182b4e" : "white",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setRole(USER_ROLES.RH);
                    }}
                  >
                    {USER_ROLES.RH}
                  </div>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: "1px solid #182b4e",
                      flex: 1,
                      width: "32px",
                      textAlign: "center",
                      color: role === USER_ROLES.SU ? "white" : "#182b4e",
                      backgroundColor:
                        role === USER_ROLES.SU ? "#182b4e" : "white",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setRole(USER_ROLES.SU);
                    }}
                  >
                    {USER_ROLES.SU}
                  </div>
                </FlexRow>
                <FlexRow style={{ gap: "8px", justifyContent: "space-evenly" }}>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: "1px solid #182b4e",
                      flex: 1,
                      width: "32px",
                      textAlign: "center",
                      color: role === USER_ROLES.SH ? "white" : "#182b4e",
                      backgroundColor:
                        role === USER_ROLES.SH ? "#182b4e" : "white",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setRole(USER_ROLES.SH);
                    }}
                  >
                    {USER_ROLES.SH}
                  </div>
                  <div
                    style={{
                      padding: "4px",
                      borderRadius: "4px",
                      border: "1px solid #182b4e",
                      flex: 1,
                      width: "32px",
                      textAlign: "center",
                      color: role === USER_ROLES.SA ? "white" : "#182b4e",
                      backgroundColor:
                        role === USER_ROLES.SA ? "#182b4e" : "white",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      setRole(USER_ROLES.SA);
                    }}
                  >
                    {USER_ROLES.SA}
                  </div>
                </FlexRow>

                {error && <ErrorText>{error}</ErrorText>}
                <button
                  style={{
                    width: "100%",
                    backgroundColor: "#182b4e",
                    height: "40px",
                    borderRadius: "4px",
                    color: "white",
                    cursor: "pointer",
                    fontSize: "24px",
                    outlineOffset: "4px",
                    outline: "1px solid #182b4e",
                    marginBottom: "16px",
                  }}
                  onClick={() => onSubmit()}
                  disabled={loading}
                >
                  <OutfitText style={{ fontSize: "24px" }}>Submit</OutfitText>
                </button>
              </FlexColumn>
            </FlexColumn>
          </Modal>
        )}
        <FlexColumn style={{ alignItems: "center", gap: "24px" }}>
          <img
            src={
              "https://storage.googleapis.com/react-static/logo_dark_512.png"
            }
            alt=""
            style={{
              height: isMobile ? "80px" : "120px",
              width: isMobile ? "80px" : "120px",
            }}
          />
        </FlexColumn>
        <FlexColumn>
          <FlexRow style={{ justifyContent: "flex-end" }}>
            <button
              style={{
                width: "100%",
                maxWidth: "120px",
                backgroundColor: "#264aa5",
                borderRadius: "4px",
                color: "white",
                cursor: "pointer",
                fontSize: "24px",
                marginBottom: "16px",
              }}
              onClick={() => dispatch(toggleAddAdminModalOpen(true))}
              disabled={loading}
              // onClick={() => console.log(isValid(content))}
            >
              <FlexRow
                style={{ alignItems: "center", justifyContent: "space-evenly" }}
              >
                <OutfitText style={{ fontSize: "32px" }}>+</OutfitText>
                <OutfitText style={{ fontSize: "24px" }}>Add</OutfitText>
              </FlexRow>
            </button>
          </FlexRow>
          <ListComponent
            loading={loading}
            columnHeaders={[
              { text: "Name", flexValue: "0.25" },
              { text: "Phone", flexValue: "0.25" },
              { text: "Role", flexValue: "0.2" },
              { text: "Created", flexValue: "0.15" },
              { text: "", flexValue: "0.15" },
            ]}
            columnValues={admins.map((admin) => {
              const name = admin.name;
              const phone = admin.phone;
              const createdOn = admin.timestamp;
              const enabled = admin.enabled;
              const role = admin.role;

              return [
                {
                  content: (
                    <OutfitText
                      style={{ fontSize: isMobile ? "12px" : "24px" }}
                    >
                      {name}
                    </OutfitText>
                  ),
                  flexValue: 0.25,
                },
                {
                  content: (
                    <OutfitText
                      style={{ fontSize: isMobile ? "12px" : "24px" }}
                    >
                      {phone}
                    </OutfitText>
                  ),
                  flexValue: 0.25,
                },
                {
                  content: (
                    <OutfitText
                      style={{
                        fontSize: isMobile ? "12px" : "24px",
                      }}
                    >
                      {role}
                    </OutfitText>
                  ),
                  flexValue: 0.2,
                },
                {
                  content: (
                    <OutfitText
                      style={{ fontSize: isMobile ? "12px" : "24px" }}
                    >
                      {moment(createdOn).format("DD/MM/YYYY")}
                    </OutfitText>
                  ),
                  flexValue: 0.15,
                },
                {
                  content: (
                    <FlexRow
                      style={{
                        width: "100%",
                        justifyContent: "center",
                      }}
                    >
                      <Switch
                        checked={enabled}
                        onChange={() =>
                          enabled
                            ? dispatch(disableAdmin(phone))
                            : dispatch(enableAdmin(phone))
                        }
                        style={{
                          background: enabled ? "#264aa5" : "#d8d8d8",
                        }}
                        size={isMobile ? "small" : "default"}
                      />
                    </FlexRow>
                  ),
                  flexValue: 0.15,
                },
              ];
            })}
          />
        </FlexColumn>
      </PageContainer>
    </>
  );
};

export default Admin;
