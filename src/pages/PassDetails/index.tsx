import { isMobile } from "react-device-detect";
import { FlexColumn, PageContainer } from "../../components/commonStyles";
import OYW21 from "../../images/icon-192x192.png";
import { useParams } from "react-router";
import QRCode from "react-qr-code";
import { NavHeaderText, OutfitText } from "../../components/commonText";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { useEffect } from "react";
import { getPassDetails } from "../../store/passSlice";
import ListComponent from "../../components/ListComponent";
import { Link } from "react-router-dom";

const PassDetails = () => {
  const dispatch = useAppDispatch();

  const { passDetails: code }: { passDetails?: string } = useParams();
  useEffect(() => {
    dispatch(getPassDetails({ code: code! }));
  }, []);

  const { passDetails, loading } = useAppSelector(
    (state) => state.entities.pass
  );
  return (
    <PageContainer style={{ padding: isMobile ? "16px" : "48px" }}>
      <FlexColumn style={{ alignItems: "center", gap: "24px" }}>
        <img src={OYW21} alt="" style={{ height: "120px", width: "120px" }} />
        {!loading && passDetails.short_code ? (
          <>
          <OutfitText>Pass #{passDetails.pass_number}</OutfitText>
        <QRCode value={code!} />
        <NavHeaderText>Short Code: {passDetails.short_code}</NavHeaderText>
          <FlexColumn
            style={{
              width: isMobile ? "100%" : "560px",
              marginTop: "16px",
              gap: "16px",
            }}
          >
            <OutfitText style={{ fontSize: "28px", alignSelf: "center" }}>
              Hello {passDetails.name}!
            </OutfitText>
            <ListComponent
              loading={loading}
              columnHeaders={[
                { text: "Day", flexValue: "0.3" },
                { text: "Booked", flexValue: "0.25" },
                { text: "Redeemed", flexValue: "0.25" },
                { text: "Balance", flexValue: "0.25" },
              ]}
              columnValues={[
                [
                  {
                    content: (
                      <OutfitText
                        style={{ fontSize: isMobile ? "20px" : "24px" }}
                      >
                        Singing
                      </OutfitText>
                    ),
                    flexValue: 0.4,
                  },
                  {
                    content: (
                      <OutfitText>{passDetails.singing_count}</OutfitText>
                    ),
                    flexValue: 0.25,
                  },
                  {
                    content: (
                      <OutfitText>
                        {passDetails.singing_redeemed_count}
                      </OutfitText>
                    ),
                    flexValue: 0.25,
                  },
                  {
                    content: (
                      <OutfitText>
                        {passDetails.singing_count -
                          passDetails.singing_redeemed_count}
                      </OutfitText>
                    ),
                    flexValue: 0.25,
                  },
                ],
                [
                  {
                    content: (
                      <OutfitText
                        style={{ fontSize: isMobile ? "20px" : "24px" }}
                      >
                        Drama
                      </OutfitText>
                    ),
                    flexValue: 0.4,
                  },
                  {
                    content: <OutfitText>{passDetails.drama_count}</OutfitText>,
                    flexValue: 0.25,
                  },
                  {
                    content: (
                      <OutfitText>
                        {passDetails.drama_redeemed_count}
                      </OutfitText>
                    ),
                    flexValue: 0.25,
                  },
                  {
                    content: (
                      <OutfitText>
                        {passDetails.drama_count +
                          passDetails.drama_redeemed_count}
                      </OutfitText>
                    ),
                    flexValue: 0.25,
                  },
                ],
                [
                  {
                    content: (
                      <OutfitText
                        style={{ fontSize: isMobile ? "20px" : "24px" }}
                      >
                        Dance
                      </OutfitText>
                    ),
                    flexValue: 0.4,
                  },
                  {
                    content: <OutfitText>{passDetails.dance_count}</OutfitText>,
                    flexValue: 0.25,
                  },
                  {
                    content: (
                      <OutfitText>
                        {passDetails.dance_redeemed_count}
                      </OutfitText>
                    ),
                    flexValue: 0.25,
                  },
                  {
                    content: (
                      <OutfitText>
                        {passDetails.dance_count +
                          passDetails.dance_redeemed_count}
                      </OutfitText>
                    ),
                    flexValue: 0.25,
                  },
                ],
              ]}
            />
          </FlexColumn>
          </>
        ) : (
          <FlexColumn  style={{ alignItems: "center", gap: "24px" }}>
          <OutfitText>
            This pass doesn't exist.
          </OutfitText>
          
          <OutfitText style={{fontSize:"20px" }}>
            Book tickets for OYW 2021 at the registration desk in front of the Church, after the evening masses.
          </OutfitText>
            or<br />
          <OutfitText style={{fontSize:"20px" }}>
            Watch OYW 2021 live streamed <Link to="/livestream">here</Link>
          </OutfitText>
          <hr />
          <OutfitText style={{fontSize:"20px", fontWeight: "bold" }}>
            Having E-ticket issues? Contact us @ <a href="mailto:orlemyouthweek21@gmail.com">orlemyouthweek21@gmail.com</a>
          </OutfitText>
          <hr/>
          <FlexColumn style={{padding: "30px", alignItems: "center", fontSize: "18px"}}>
              <OutfitText>Guidelines for OYW</OutfitText><br />
              <ol>
                <li>Each QR Pass is issued to one person only (on one phone number / email).</li> 
                <li>A single person may purchase passes for several people on the same QR (unique) and have the count decremented on scanning upon entering the venue.</li> 
                <li>Children and senior citizens are advised to watch the live stream from the comfort of their homes for their own safety.</li> 
                <li>Kindly wear your masks at all times. Attending the event without masks, will not be permitted.</li> 
                <li>Avoid crowding or forming groups at any place during the event.</li> 
                <li>Please follow the COVID-19 guidelines for your own safety and safety of the others attending the event.</li> 
                <li>Anyone found breaching the Covid-19 guidelines will be escorted out.</li> 
                <li>The church is not responsible for the health and safety of anyone attending the event.</li>
              </ol>
              By buying your QR Pass you agree to all the rules and regulations mentioned above and accept to follow them.
              </FlexColumn>
          </FlexColumn>
        )}
      </FlexColumn>
    </PageContainer>
  );
};

export default PassDetails;
