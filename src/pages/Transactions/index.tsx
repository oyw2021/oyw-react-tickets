import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import {
  FlexColumn,
  FlexRow,
  PageContainer,
  SearchBarContainer,
} from "../../components/commonStyles";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import Loader from "../../components/Loader";
import ListComponent from "../../components/ListComponent";
import { OutfitText } from "../../components/commonText";
import "antd/dist/antd.css";
import cookie from "react-cookies";
import { Navigate } from "react-router";
import { USER_ROLES } from "../../utils/constants";
import SearchIcon from "@mui/icons-material/Search";
import {
  getTransactions,
  setUndoModal,
  setUndoScanModalOpen,
  undoScan,
  undoTransaction,
} from "../../store/passSlice";
import Modal from "../../components/Modal";
import Close from "../../images/Close.png";
import Navbar from "../../components/TicketsNavbar";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import moment from "moment-timezone";


const Transactions = () => {
  const user_role: string = cookie.load("user_role");
  const token: string = cookie.load("token");

  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getTransactions());
  }, []);

  const {
    loading,
    transaction,
    undoModalOpen,
    scanTransaction,
    undoScanModalOpen,
  } = useAppSelector((state) => state.entities.pass);
  const [scanOpen, setScanOpen] = useState<boolean>(false);
  const [registrationOpen, setRegistrationOpen] = useState<boolean>(false);
  const [undoEmail, setUndoEmail] = useState<string>("");
  const [undoDay1, setUndoDay1] = useState<number>(0);
  const [undoDay2, setUndoDay2] = useState<number>(0);
  const [undoDay3, setUndoDay3] = useState<number>(0);
  
  let totalAmount = 0
  let totalSinging = 0
  let totalDrama = 0
  let totalDance = 0


  const [undoTransactionId, setUndoTransactionId] = useState<string>("");
  const [searchText, setSearchText] = useState<string>("");
  let filteredTransactionList = transaction;
  let filteredScanTransaction = scanTransaction;
  if (searchText) {
    if (scanOpen) {
      filteredScanTransaction = scanTransaction.filter(
        (trxn) =>
          trxn.email.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    } else {
      filteredTransactionList = transaction.filter(
        (trxn) =>
          trxn.email.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
      );
    }
  }

  if (user_role === undefined) return <Navigate to="/tickets/signin" />;
  return loading ? (
    <Loader />
  ) : (
    <>
      <Navbar />
      {undoModalOpen && (
        <Modal width={isMobile ? "90%" : "480px"}>
          <FlexColumn style={{ gap: "16px" }}>
            <FlexRow style={{ justifyContent: "flex-end" }}>
              <img
                src={Close}
                alt=""
                style={{ width: "24px", height: "24px" }}
                onClick={() => dispatch(setUndoModal(false))}
              />
            </FlexRow>
            <FlexColumn
              style={{ alignItems: "center", width: "100%", gap: "16px" }}
            >
              <FlexColumn style={{ alignItems: "center" }}>
                <OutfitText style={{ fontSize: "20px" }}>
                  Undo {undoEmail}'s registration.
                </OutfitText>
                {undoDay1 !== 0 && (
                  <OutfitText style={{ fontSize: "18px" }}>
                    Singing - {undoDay1}
                  </OutfitText>
                )}
                {undoDay2 !== 0 && (
                  <OutfitText style={{ fontSize: "18px" }}>
                    Drama - {undoDay2}
                  </OutfitText>
                )}
                {undoDay3 !== 0 && (
                  <OutfitText style={{ fontSize: "18px" }}>
                    Dance - {undoDay3}
                  </OutfitText>
                )}
              </FlexColumn>
              <FlexRow style={{ width: "100%", gap: "16px" }}>
                <button
                  style={{
                    width: "100%",
                    backgroundColor: "#182b4e",
                    height: "40px",
                    borderRadius: "4px",
                    color: "white",
                    cursor: "pointer",
                    fontSize: "24px",
                    marginBottom: "16px",
                    padding: "0px auto",
                  }}
                  // onClick={() => onSubmit(mobile, passcode)}
                  disabled={loading}
                  // onClick={() => console.log(isValid(content))}
                  onClick={() => {
                    dispatch(setUndoModal(false));
                    setUndoEmail("");
                  }}
                >
                  <OutfitText style={{ fontSize: "20px" }}>Cancel</OutfitText>
                </button>
                <button
                  style={{
                    width: "100%",
                    backgroundColor: "#182b4e",
                    height: "40px",
                    borderRadius: "4px",
                    color: "white",
                    cursor: "pointer",
                    fontSize: "24px",
                    marginBottom: "16px",
                    padding: "0px auto",
                  }}
                  // onClick={() => onSubmit(mobile, passcode)}
                  disabled={loading}
                  onClick={() => {
                    dispatch(undoTransaction(undoTransactionId));
                  }}
                >
                  <OutfitText style={{ fontSize: "20px" }}>Confirm</OutfitText>
                </button>
              </FlexRow>
            </FlexColumn>
          </FlexColumn>
        </Modal>
      )}
      {undoScanModalOpen && (
        <Modal width={isMobile ? "90%" : "480px"}>
          <FlexColumn style={{ gap: "16px" }}>
            <FlexRow style={{ justifyContent: "flex-end" }}>
              <img
                src={Close}
                alt=""
                style={{ width: "24px", height: "24px" }}
                onClick={() => dispatch(setUndoScanModalOpen(false))}
              />
            </FlexRow>
            <FlexColumn
              style={{ alignItems: "center", width: "100%", gap: "16px" }}
            >
              <FlexColumn style={{ alignItems: "center" }}>
                <OutfitText style={{ fontSize: "20px" }}>
                  Undo {undoEmail}'s scan.
                </OutfitText>
              </FlexColumn>
              <FlexRow style={{ width: "100%", gap: "16px" }}>
                <button
                  style={{
                    width: "100%",
                    backgroundColor: "#182b4e",
                    height: "40px",
                    borderRadius: "4px",
                    color: "white",
                    cursor: "pointer",
                    fontSize: "24px",
                    marginBottom: "16px",
                    padding: "0px auto",
                  }}
                  // onClick={() => onSubmit(mobile, passcode)}
                  disabled={loading}
                  // onClick={() => console.log(isValid(content))}
                  onClick={() => {
                    dispatch(setUndoScanModalOpen(false));
                    setUndoEmail("");
                  }}
                >
                  <OutfitText style={{ fontSize: "20px" }}>Cancel</OutfitText>
                </button>
                <button
                  style={{
                    width: "100%",
                    backgroundColor: "#182b4e",
                    height: "40px",
                    borderRadius: "4px",
                    color: "white",
                    cursor: "pointer",
                    fontSize: "24px",
                    marginBottom: "16px",
                    padding: "0px auto",
                  }}
                  // onClick={() => onSubmit(mobile, passcode)}
                  disabled={loading}
                  onClick={() => {
                    dispatch(undoScan(undoTransactionId));
                  }}
                >
                  <OutfitText style={{ fontSize: "20px" }}>Confirm</OutfitText>
                </button>
              </FlexRow>
            </FlexColumn>
          </FlexColumn>
        </Modal>
      )}
      <PageContainer style={{ padding: isMobile ? "16px" : "48px" }}>
        <FlexColumn
          style={{
            alignItems: "center",
            gap: "24px",
            width: "100%",
          }}
        >
          <img
            src={
              "https://storage.googleapis.com/react-static/logo_dark_512.png"
            }
            alt=""
            style={{
              height: isMobile ? "80px" : "120px",
              width: isMobile ? "80px" : "120px",
            }}
          />
          <SearchBarContainer
            style={{ alignSelf: "flex-start", width: isMobile ? "" : "40%" }}
          >
            <SearchIcon />
            <input
              type="text"
              name="search"
              id="search"
              placeholder="Search"
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
            />
          </SearchBarContainer>
          <FlexColumn style={{ width: "100%" }}>
            <FlexColumn style={{ width: "100%" }}>
              <FlexRow
                style={{
                  justifyContent: "space-between",
                  padding: "16px",
                  cursor: "pointer",
                }}
                onClick={() => setRegistrationOpen(!registrationOpen)}
              >
                <OutfitText style={{ fontSize: "24px" }}>
                  Registration Transactions
                </OutfitText>
                {registrationOpen ? <KeyboardArrowDownIcon /> : <KeyboardArrowUpIcon />}
              </FlexRow>
              {!registrationOpen && (
                <ListComponent
                  loading={loading}
                  columnHeaders={[
                    { text: "Sr No", flexValue: "0.1" },
                    { text: "Email", flexValue: "0.4" },
                    { text: "Day 1", flexValue: "0.1" },
                    { text: "Day 2", flexValue: "0.1" },
                    { text: "Day 3", flexValue: "0.1" },
                    { text: "Timestamp", flexValue: "0.2" },
                  ]}
                  columnValues={filteredTransactionList.slice(0).reverse().map((trxn, index) => {
                    const sr_no = filteredTransactionList.length - index;
                    const email = trxn.email;
                    const day1 = trxn.singing_count;
                    const day2 = trxn.drama_count;
                    const day3 = trxn.dance_count;
                    const amount = trxn.amount;
                    const timestamp = trxn.timestamp;
                    const id = trxn.id;

                    return [
                      {
                    content: (
                          <OutfitText
                            style={{ fontSize: isMobile ? "10px" : "24px" }}
                          >
                            {sr_no}
                          </OutfitText>
                        ),
                        flexValue: 0.1,
                        },{
                        content: (
                          <OutfitText
                            style={{
                              fontSize: isMobile ? "10px" : "24px",
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              if (user_role === USER_ROLES.SA) {
                                dispatch(setUndoModal(true));
                                setUndoEmail(email);
                                setUndoTransactionId(id);
                                setUndoDay1(day1);
                                setUndoDay2(day2);
                                setUndoDay3(day3);
                              }
                            }}
                          >
                            {email}
                          </OutfitText>
                        ),
                        flexValue: 0.4,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{ fontSize: isMobile ? "10px" : "24px" }}
                          >
                            {day1}
                          </OutfitText>
                        ),
                        flexValue: 0.1,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{
                              fontSize: isMobile ? "10px" : "24px",
                            }}
                          >
                            {day2}
                          </OutfitText>
                        ),
                        flexValue: 0.1,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{
                              fontSize: isMobile ? "10px" : "24px",
                            }}
                          >
                            {day3}
                          </OutfitText>
                        ),
                        flexValue: 0.1,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{
                              fontSize: isMobile ? "10px" : "24px",
                            }}
                          >
                            {moment(timestamp).format(
                              "DD/MM~hh:mm:ss"
                            )}
                          </OutfitText>
                        ),
                        flexValue: 0.2,
                      },
                    ];
                  })}
                />
              )}
            </FlexColumn>
            <FlexRow
              style={{
                justifyContent: "space-between",
                padding: "16px",
                cursor: "pointer",
              }}
              onClick={() => setScanOpen(!scanOpen)}
            >
              <OutfitText style={{ fontSize: "24px" }}>
                Scanned Transactions
              </OutfitText>
              {!scanOpen ? <KeyboardArrowDownIcon /> : <KeyboardArrowUpIcon />}
            </FlexRow>
            {scanOpen && (
              <FlexColumn style={{ width: "100%" }}>
                <ListComponent
                  loading={loading}
                  columnHeaders={[
                    { text: "Sr No", flexValue: "0.1" },
                    { text: "Email", flexValue: "0.4" },
                    { text: "Day", flexValue: "0.1" },
                    { text: "Count", flexValue: "0.1" },
                    { text: "Timestamp", flexValue: "0.3" }
                  ]}
                  columnValues={filteredScanTransaction.slice(0).reverse().map((trxn, index) => {
                    const sr_no = filteredScanTransaction.length - index;
                    const email = trxn.email;
                    const day = trxn.day;
                    const count = trxn.count;
                    const timestamp = trxn.timestamp;
                    const amount = trxn.amount;
                    const id = trxn.id;

                    return [
                      {
                        content: (
                          <OutfitText
                            style={{ fontSize: isMobile ? "12px" : "24px" }}
                          >
                            {sr_no}
                          </OutfitText>
                        ),
                        flexValue: 0.1,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{
                              fontSize: isMobile ? "10px" : "24px",
                              cursor: "pointer",
                            }}
                            onClick={() => {
                              if (user_role === USER_ROLES.SA) {
                                dispatch(setUndoScanModalOpen(true));
                                setUndoEmail(email);
                                setUndoTransactionId(id);
                              }
                            }}
                          >
                            {email}
                          </OutfitText>
                        ),
                        flexValue: 0.4,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{ fontSize: isMobile ? "10px" : "24px" }}
                          >
                            {day}
                          </OutfitText>
                        ),
                        flexValue: 0.1,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{
                              fontSize: isMobile ? "10px" : "24px",
                            }}
                          >
                            {count}
                          </OutfitText>
                        ),
                        flexValue: 0.1,
                      },
                      {
                        content: (
                          <OutfitText
                            style={{
                              fontSize: isMobile ? "10px" : "24px",
                            }}
                          >
                            {moment(timestamp).format(
                              "DD/MM ~ hh:mm:ss"
                            )}
                          </OutfitText>
                        ),
                        flexValue: 0.3,
                      },
                    ];
                  })}
                />
              </FlexColumn>
            )}
          </FlexColumn>
        </FlexColumn>
        
        <OutfitText>Transactions</OutfitText>
          <OutfitText>Singing: {totalSinging}</OutfitText>
          <OutfitText>Drama: {totalDrama}</OutfitText>
          <OutfitText>Dance: {totalDance}</OutfitText>
          <OutfitText>Total Amount: {totalAmount}</OutfitText>

      </PageContainer>
    </>
  );
};

export default Transactions;