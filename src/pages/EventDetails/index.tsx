import { useParams } from "react-router";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import { FlexColumn, PageContainer } from "../../components/commonStyles";
import { EVENTS, OBJECT_STORE_URL } from "../../utils/constants";
import { isMobile } from "react-device-detect";
import { OutfitText } from "../../components/commonText";
import OYW21 from "../../images/icon-192x192.png";
import Navbar from "../../components/TicketsNavbar";


const EventDetails = () => {
  const { name }: { name?: string } = useParams();
  const eventDetails = EVENTS.DETAILS.find((event) => event.id === name);
  const getConfigurableProps = () => ({
    autoPlay: true,
    infiniteLoop: true,
    interval: 2000,
    showThumbs: false,
    showIndicators: !isMobile,
  });

  return eventDetails ? (
    <PageContainer style={{ padding: isMobile ? "16px" : "48px" }}>
      <Navbar />
      <FlexColumn style={{ alignItems: "center", gap: "24px" }}>
        <img src={OYW21} alt="" style={{ height: "120px", width: "120px" }} />
        <OutfitText
          style={{
            fontSize: isMobile ? "16px" : "48px",
          }}
        >{`Day ${eventDetails.day}`}</OutfitText>
        <OutfitText
          style={{
            color: "#f9a444",
            border: "2px solid #bf8d6f",
            padding: "8px 16px",
            margin: "0 auto",
            textAlign: "center",
          }}
        >{`${eventDetails.theme}`}</OutfitText>
        <OutfitText>{eventDetails.name}</OutfitText>
        <FlexColumn style={{ alignItems: "center" }}>
          <Carousel
            width={!isMobile ? "900px" : "100%"}
            {...getConfigurableProps()}
          >
            {eventDetails.images.map((image) => (
              <img
                src={OBJECT_STORE_URL + image}
                alt=""
                style={{
                  width: isMobile ? "100%" : "900px",
                  height: isMobile ? "" : "600px",
                  objectFit: "contain",
                }}
              />
            ))}
          </Carousel>
          <OutfitText
            style={{
              fontSize: "24px",
              textAlign: "justify",
              maxWidth: isMobile ? "100%" : "900px",
            }}
          >
            {eventDetails.description}
          </OutfitText>
        </FlexColumn>
      </FlexColumn>
    </PageContainer>
  ) : (
    <></>
  );
};

export default EventDetails;
