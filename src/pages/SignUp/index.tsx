import { useState } from "react";
import {
  FlexColumn,
  FlexRow,
  LoginTextInput,
  PageContainer,
  WhiteCard,
} from "../../components/commonStyles";
import {
  ErrorText,
  NavHeaderText,
  OutfitText,
} from "../../components/commonText";
import Joi from "joi";
import { setLoading, signIn } from "../../store/homeSlice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { isMobile } from "react-device-detect";
import OYW21 from "../../images/logo_light_512.png";
import { useNavigate } from "react-router-dom";
import Loader from "../../components/Loader";
import Navbar from "../../components/TicketsNavbar";


const SignUpCard: React.FC<{ title: string }> = ({ title }) => {
  const dispatch = useAppDispatch();
  const { loading } = useAppSelector((state) => state.entities.home);
  const [mobile, setMobile] = useState<string>("");
  const [passcode, setPasscode] = useState<string>("");
  const [error, setError] = useState<string>("");
  const onSubmit = (phone: string, password: string) => {
    const validate = SignUpValidateion.validate({
      mobile: phone,
      passcode: password,
    });

    if (validate.error) {
      setError(validate.error.message);
    } else {
      setError("");
      dispatch(signIn(phone, password));
    }
  };

  return (
    <WhiteCard
      style={
        isMobile
          ? { margin: "10%", maxHeight: "50%", padding: "20px" }
          : { width: "400px", maxHeight: "400px", padding: "40px" }
      }
    >
      <FlexColumn
        style={{
          gap: "32px",
          height: "100%",
        }}
      >
        <OutfitText style={{ fontSize: "32px" }}>{title}</OutfitText>
        <FlexColumn
          style={{
            justifyContent: "flex-end",
            height: "100%",
            gap: "16px",
          }}
        >
          <LoginTextInput
            placeholder="Mobile"
            onChange={(e) => setMobile(e.target.value)}
            value={mobile}
          />
          <LoginTextInput
            type="password"
            placeholder="Passcode"
            onChange={(e) => setPasscode(e.target.value)}
            onKeyUp={(e) => {
              if (e.keyCode === 13) {
                e.preventDefault();
                onSubmit(mobile, passcode);
              }
            }}
            value={passcode}
          />
          {error && <ErrorText>{error}</ErrorText>}
          <button
            style={{
              width: "100%",
              backgroundColor: "#182b4e",
              height: "40px",
              borderRadius: "24px",
              color: "white",
              cursor: "pointer",
              fontSize: "24px",
              outline: "2.4px solid #182b4e",
              outlineOffset: "4px",
            }}
            onClick={() => onSubmit(mobile, passcode)}
            disabled={loading}
          >
            <OutfitText style={{ fontSize: "24px" }}>Submit</OutfitText>
          </button>
        </FlexColumn>
      </FlexColumn>
    </WhiteCard>
  );
};

const SignUpValidateion = Joi.object()
  .keys({
    mobile: Joi.string()
      .length(10)
      .pattern(/^[0-9]+$/),
    passcode: Joi.string().min(8),
  })
  .required();

const Login = () => {
  const navigate = useNavigate();
  const { loading } = useAppSelector((state) => state.entities.home);

  return loading ? (
    <Loader />
  ) : (
    <>
    <Navbar />
    <PageContainer style={{height: "100vh", backgroundColor: "#000"}}>
      {isMobile ? (
        <FlexColumn
          style={{
            backgroundColor: "#000",
            width: "100%",
            minHeight: "100%"
          }}
        >
          {/* <img src={} alt="" /> */}
          <img
            src={OYW21}
            alt=""
            style={{ height: "50vw", width: "50vw", alignSelf: "center" }}
          />
          <SignUpCard title="Sign In" />
        </FlexColumn>
      ) : (
        <FlexColumn 
        style={{
          backgroundColor: "#000",
          width: "100%",
          height: "100%"
        }}><FlexRow
            style={{
              width: "100%",
              height: "100%",
              justifyContent: "center",
            }}
          >
            <FlexColumn
              style={{
                flex: 0.4, backgroundColor: "#000",
                alignItems: "flex-end",
                justifyContent: "center",
              }}
            >
              <img
                src={OYW21}
                alt=""
                style={{ height: "400px", width: "400px" }}
              />
            </FlexColumn>
            <div style={{ flex: 0.6, backgroundColor: "#000"}}>
              <FlexColumn
                style={{
                  height: "100%",
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <SignUpCard title="Sign In" />
              </FlexColumn>
            </div>
          </FlexRow>
        </FlexColumn>
      )}
    </PageContainer>
    </>
  );
};

export default Login;
