import "./index.css";
import {
  FlexColumn,
  GradientBackgroundContainer,
  FlexRow,
  LiveStreamButton,
} from "../../components/commonStyles";
import { TITLE } from "../../utils/constants";
import Navbar from "../../components/TicketsNavbar";
import QRCode from "react-qr-code";

import EventCard from "../../components/EventCard";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { Link } from "react-router-dom";
import { OutfitText } from "../../components/commonText";
import { isMobile } from "react-device-detect";
import KeyboardArrowDownSharpIcon from "@mui/icons-material/KeyboardArrowDownSharp";
import { IconButton } from "@mui/material";
import { useEffect, useRef } from "react";
import { getPassesCounts } from "../../store/homeSlice";

import insta_logo from "../../images/logo-instagram.png"


const LandingPage = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getPassesCounts());
  }, []);
  const { passesCounts, events, loading } = useAppSelector(
    (state) => state.entities.home
  );
  const { day1, day2, day3 } = passesCounts;

  const eventsRef = useRef<HTMLDivElement>(null);
  const toEvents = () => {
    eventsRef.current!.scrollIntoView({
      behavior: "smooth",
    });
  };

  return (
    <FlexColumn style={{ maxWidth: "100vw" }}>
      <GradientBackgroundContainer>
        <FlexColumn
          style={{
            minHeight: "100vh",
            // gap:"120px"
          }}
        >
          <FlexRow style={{ width: "100%", backgroundColor: "#552c2c4e" }}>
            <Navbar />
          </FlexRow>
          <FlexColumn
            style={{
              justifyContent: "center",
              alignItems: "center",
              minHeight: "80vh",
              gap: "48px",
            }}
          >
            <FlexColumn style={{ alignItems: "center" }}>
              <img
                src={
                  "https://storage.googleapis.com/react-static/logo_dark_512.png"
                }
                alt=""
                style={{ height: "200px", width: "200px" }}
              />
              <OutfitText style={{ fontWeight: "bold" }}>{TITLE}</OutfitText>
            </FlexColumn>
            <Link to={"/livestream"}>
              <LiveStreamButton style={{ alignSelf: "center" }}>
                <FlexRow style={{ alignItems: "center", gap: "16px" }}>
                  <svg height="22px" width="22px" className="blinking">
                    <circle cx="10" cy="10" r="10" fill="red" />
                  </svg>
                  <OutfitText
                    style={{
                      fontSize: "24px",
                      color: "white",
                      fontWeight: 600,
                    }}
                  >
                    WATCH LIVE!
                  </OutfitText>
                </FlexRow>
              </LiveStreamButton>
            </Link>
            <IconButton size="large" onClick={() => toEvents()}>
              <KeyboardArrowDownSharpIcon
                style={{ color: "white", fontSize: "40px" }}
              />
            </IconButton>
          </FlexColumn>
        </FlexColumn>
      </GradientBackgroundContainer>
      <FlexRow
        style={{
          justifyContent: "center",
          gap: "32px",
          flexDirection: isMobile ? "column" : "row",
          alignItems: "center",
        }}
        id="events"
        ref={eventsRef}
      >
        <OutfitText style={{ fontSize: "40px" }}>Events</OutfitText>
      </FlexRow>
      <FlexRow
        style={{
          justifyContent: "center",
          gap: "32px",
          flexDirection: isMobile ? "column" : "row",
          alignItems: "center",
        }}
        id="events"
        ref={eventsRef}
      >
        {!loading &&
          events.map((event, index) => (
            <Link
              to={`${event.name}`}
              style={{ textDecoration: "none", color: "initial" }}
            >
              <EventCard
                {...event}
                day={index + 1}
                count={0}
              />
            </Link>
          ))}
      </FlexRow>
      <FlexRow style={{ justifyContent: "center", width: "100%" }}>
        <video width="350" height="300" controls autoPlay>
          <source
            src="https://storage.googleapis.com/ads-oyw/pcpl_compressed.mp4"
            type="video/mp4"
          />
        </video>
      </FlexRow>
      <br />
      <br />
      <FlexColumn
        style={{
          padding: "30px",
          alignItems: "center",
          fontSize: "18px",
          color: "#fff",
          backgroundColor: "#282828",
        }}
      >
        <OutfitText>Guidelines for OYW</OutfitText>
        <br />
        <ol>
          <li>
            Each QR Pass is issued to one person only (on one phone number /
            email).
          </li>
          <li>
            A single person may purchase passes for several people on the same
            QR (unique) and have the count decremented on scanning upon entering
            the venue.
          </li>
          <li>
            Children and senior citizens are advised to watch the live stream
            from the comfort of their homes for their own safety.
          </li>
          <li>
            Kindly wear your masks at all times. Attending the event without
            masks, will not be permitted.
          </li>
          <li>
            Avoid crowding or forming groups at any place during the event.
          </li>
          <li>
            Please follow the COVID-19 guidelines for your own safety and safety
            of the others attending the event.
          </li>
          <li>
            Anyone found breaching the Covid-19 guidelines will be escorted out.
          </li>
          <li>
            The church is not responsible for the health and safety of anyone
            attending the event.
          </li>
        </ol>
        By buying your QR Pass you agree to all the rules and regulations
        mentioned above and accept to follow them.
      </FlexColumn>
      <FlexColumn
        style={{
          justifyContent: "center",
          gap: "32px",
          padding: "50px",
          flexDirection: isMobile ? "column" : "row",
          alignItems: "center",
        }}
      >
        <a target="_blank" href="https://www.instagram.com/youthoforlem/">
          <OutfitText
            style={{
              fontSize: "20px",
              fontWeight: 600,
            }}
          >
            Follow us on Instagram! &nbsp;
            <img src={insta_logo} height="40px" />
          </OutfitText>
        </a>
      </FlexColumn>
    </FlexColumn>
  );
};

export default LandingPage;
