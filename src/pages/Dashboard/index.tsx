import React, { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import { Link, Navigate } from "react-router-dom";
import {
  CountContainer,
  FlexColumn,
  FlexRow,
  OutlinedButton,
  PageContainer,
} from "../../components/commonStyles";
import { OutfitText } from "../../components/commonText";
import { getPassesCounts } from "../../store/homeSlice";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import cookie from "react-cookies";
import { USER_ROLES } from "../../utils/constants";
import Navbar from "../../components/TicketsNavbar";

const SoldContainer: React.FC<{
  day: number;
  name: string;
  count: number;
  redeemedCount: number;
}> = ({ day, name, count, redeemedCount }) => {
  return (
    <CountContainer
      style={{
        border: "1px solid #182b4e",
        padding: "16px",
        borderRadius: "8px",
        flex: 1,
        boxShadow: isMobile
          ? "rgb(85, 91, 255) 0px 0px 0px 3px, rgb(31, 193, 27) 0px 0px 0px 6px, rgb(255, 217, 19) 0px 0px 0px 9px, rgb(255, 156, 85) 0px 0px 0px 12px, rgb(255, 85, 85) 0px 0px 0px 15px"
          : "",
      }}
    >
      <FlexColumn
        style={{
          alignItems: "center",
          justifyContent: "space-between",
          height: "100%",
        }}
      >
        <FlexColumn style={{ flex: 0.3 }}>
          <OutfitText
            style={{
              fontSize: "32px",
              color: "#f9a444",
              margin: "0 auto",
              padding: "4px 16px",
              fontWeight: 600,
            }}
          >
            DAY {day}
          </OutfitText>
          <OutfitText style={{ fontWeight: 100 }}>{name}</OutfitText>
        </FlexColumn>
        <FlexColumn style={{ flex: 0.3 }}>
          <OutfitText style={{ fontSize: "24px" }}>Sold: {count}</OutfitText>
          <OutfitText style={{ fontSize: "24px" }}>
            Redeemed: {redeemedCount}
          </OutfitText>
        </FlexColumn>
        {/* <FlexColumn style={{ flex: 0.4 }}>
                <OutfitText style={{ fontSize: "56px" }}>{count}</OutfitText>
              </FlexColumn> */}
      </FlexColumn>
    </CountContainer>
  );
};

const Dashboard = () => {
  const user_role: string = cookie.load("user_role");
  const token: string = cookie.load("token");
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getPassesCounts());
  }, []);
  const { passesCounts, loading } = useAppSelector(
    (state) => state.entities.home
  );
  const [count1, setCount1] = useState<number>(0);
  const [count2, setCount2] = useState<number>(0);
  const [count3, setCount3] = useState<number>(0);
  const [redeemedCount1, setRedeemedCount1] = useState<number>(0);
  const [redeemedCount2, setRedeemedCount2] = useState<number>(0);
  const [redeemedCount3, setRedeemedCount3] = useState<number>(0);

  function animateValue(
    start: number,
    end: number,
    duration: number,
    setCount: (z: number) => void
  ) {
    let startTimestamp: any = null;
    const step = (timestamp: any) => {
      if (!startTimestamp) startTimestamp = timestamp;
      const progress = Math.min((timestamp - startTimestamp) / duration, 1);
      setCount(Math.floor(progress * (end - start) - start));
      if (progress < 1) {
        window.requestAnimationFrame(step);
      }
    };
    window.requestAnimationFrame(step);
  }

  useEffect(() => {
    animateValue(0, passesCounts.redeemed_singing, 500, setRedeemedCount1);
    animateValue(0, passesCounts.redeemed_drama, 500, setRedeemedCount2);
    animateValue(0, passesCounts.redeemed_dance, 500, setRedeemedCount3);

    animateValue(0, passesCounts.day1, 500, setCount1);
    animateValue(0, passesCounts.day2, 500, setCount2);
    animateValue(0, passesCounts.day3, 500, setCount3);
  }, [passesCounts]);

  const [isNavOpen, setIsNavOpen] = useState<boolean>(false);
  if (user_role === undefined) return <Navigate to="/tickets/signin" />;

  return (
    <PageContainer style={{ minHeight: "100vh", height: "100%" }}>
      {!loading && (
        <FlexColumn
          style={{
            alignItems: "center",
            width: "100%",
            height: "100%",
            gap: "48px",
          }}
        >
          {/* <img src={OYW21} alt="" style={{ height: "120px", width: "120px" }} /> */}
          <Navbar />
          <FlexRow
            style={{
              height: "100%",
              width: "100%",
              padding: "5% 10%",
              gap: "48px",
              flexDirection: isMobile ? "column" : "row",
              marginBottom: "16px",
            }}
          >
            <SoldContainer
              day={1}
              count={passesCounts.day1}
              redeemedCount={passesCounts.redeemed_singing}
              name={"Singing Event"}
            />
            <SoldContainer
              day={2}
              count={passesCounts.day2}
              redeemedCount={passesCounts.redeemed_drama}
              name={"Drama Event"}
            />
            <SoldContainer
              day={3}
              count={passesCounts.day3}
              redeemedCount={passesCounts.redeemed_dance}
              name={"Dance Event"}
            />
          </FlexRow>
          {!isMobile && (
            <FlexRow
              style={{
                gap: "16px",
                width: "100%",
                justifyContent: "center",
                flexDirection: isMobile ? "column" : "row",
                alignItems: "center",
                marginTop: "16px",
                padding: "16px",
              }}
            >
              {user_role === USER_ROLES.SA && (
                <OutlinedButton
                  style={{
                    border: "3px solid #f71717",
                  }}
                >
                  <Link to="/tickets/admin">
                    <OutfitText style={{ fontSize: "16px", color: "red" }}>
                      Admin
                    </OutfitText>
                  </Link>
                </OutlinedButton>
              )}
              {(user_role !== USER_ROLES.RU || user_role !== USER_ROLES.RH) && (
                <OutlinedButton style={{ border: "3px solid green" }}>
                  <Link to="/tickets/scanner">
                    <OutfitText style={{ fontSize: "16px", color: "green" }}>
                      Scan
                    </OutfitText>
                  </Link>
                </OutlinedButton>
              )}
              {(user_role !== USER_ROLES.SU || user_role !== USER_ROLES.SH) && (
                <OutlinedButton style={{ border: "3px solid blue" }}>
                  <Link to="/tickets/register">
                    <OutfitText style={{ fontSize: "16px", color: "blue" }}>
                      Register
                    </OutfitText>
                  </Link>
                </OutlinedButton>
              )}
            </FlexRow>
          )}
        </FlexColumn>
      )}
    </PageContainer>
  );
};

export default Dashboard;
