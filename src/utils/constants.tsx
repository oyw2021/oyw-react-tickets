import { Link } from "react-router-dom";
import { NavHeaderText } from "../components/commonText";
import Dancing from "../images/dancing.jpeg";
import cookie from "react-cookies";
import Ad1 from "../images/Ads/image0.jpeg";
import Ad2 from "../images/Ads/image1.jpeg";
// import AdVideo from "../images/Ads/PCPL Ad.mp4"
export const TITLE = "ORLEM YOUTH WEEK";

export const API_SERVER_URL = "http://172.105.54.8";


export const USER_ROLES = {
  RU: "RU",
  RH: "RH",
  SU: "SU",
  SH: "SH",
  SA: "SA",
  NA: "NA",
};

export const PRICES: { [key: string]: number } = {
  "1": 80,
  "2": 80,
  "3": 100,
};

let token = cookie.load("token");

export const signOut = () => {
  console.log("Signing out...");
  cookie.remove("token");
  cookie.remove("user_role");
  window.location.reload();
};

// 1.	Each ticket is valid for 1 person only.
// 2.	Children and senior citizens are advised to watch the live stream from the comfort of their homes for their own safety.
// 3.	Kindly wear your masks at all times. No one without the mask will be permitted to attend the event.
// 4.	Avoid crowding/ forming groups at any place during the event.
// 5.	Please follow the Covid-19 guidelines for your own safety and safety of the others attending the event.
// 6.	Anyone found breaching the Covid-19 guidelines will be escorted out of the event by Security.
// 7.	The church is not responsible for the health and safety of anyone attending the event.
// By buying this ticket you agree to all the rules and regulations mentioned above and accept to follow them.

export const NAVBAR = {
  TITLE: "#OYW21",
  TILES: [
    <NavHeaderText style={{ fontSize: "24px", cursor: "pointer" }}>
      <a href="#events" style={{ textDecoration: "none", color: "inherit" }}>
        Events
      </a>
    </NavHeaderText>,
    <Link
      to={token ? "" : "/tickets/signin"}
      style={{ textDecoration: "none", color: "inherit" }}
    >
      <NavHeaderText
        style={{ fontSize: "24px", cursor: "pointer" }}
        onClick={!token ? () => {} : () => signOut()}
      >
        {token ? "Sign Out" : "Sign In"}
      </NavHeaderText>
    </Link>,
  ],
};
export const LIVE_ID = "f7ff78b0d1a842e706bf44fe95acaad6";
export const OBJECT_STORE_URL = "https://storage.googleapis.com";

export const ADS = [Ad1, Ad2];
// export const VIDEO_AD_URL = AdVideo;

export const EVENTS = {
  INDEXES: {
    singing: 0,
    dance: 1,
    drama: 2,
  },
  IMAGES: {
    dancing: Dancing,
  },
  DETAILS: [
    {
      id: "Singing",
      name: "SINGING EVENT",
      day: 1,
      description: "",
      date: "If you're searching for clues, get ready to be amused, cause its time to infuse your rhythm with some blues ! This is your chance to explore, so make sure you register yourself.",
      images: [
        "/singing-oyw/DSC_1338.jpg",
        "/singing-oyw/DSC_1344.jpg",
        "/singing-oyw/DSC_1385.jpg",
        "/singing-oyw/DSC_1407.jpg",
        "/singing-oyw/DSC_1418.jpg",
        "/singing-oyw/DSC_1435.jpg",
        "/singing-oyw/DSC_1544.jpg",
        "/singing-oyw/DSC_1554.jpg",
        "/singing-oyw/DSC_1561.jpg",
        "/singing-oyw/DSC_1565.jpg",
        "/singing-oyw/DSC_1566.jpg",
        "/singing-oyw/DSC_1589.jpg",
        "/singing-oyw/IMG_6460.jpg",
        "/singing-oyw/IMG_6481.jpg",
        "/singing-oyw/IMG_6488.jpg",
        "/singing-oyw/IMG_6516.jpg",
        "/singing-oyw/IMG_6525.jpg",
        "/singing-oyw/IMG_6565.jpg",
      ],
      theme: "RHYTHM & CLUES",
    },
    {
      id: "Dance",
      name: "DANCING EVENT",
      day: 2,
      description:
        "Emotion is the essence of movement and dancing is one of the most liberating and open ways to express emotions, allowing those feelings to move through the body, out of the body and in doing so, to move others. This year we would like to do the same, by portraying emotions through dance with a story line.",
      date: "",
      images: [
        "/general-oyw/IMG_7292.jpg",
        "/general-oyw/IMG_7360.jpg",
        "/general-oyw/IMG_7447.jpg",
        "/general-oyw/IMG_7485.jpg",
        "/general-oyw/IMG_7496.jpg",
        "/general-oyw/IMG_7530.jpg",
        "/general-oyw/IMG_7777.jpg",
        "/general-oyw/IMG_7798.jpg",
        "/general-oyw/IMG_8616.jpg",
        "/general-oyw/IMG_8617.jpg",
        "/general-oyw/IMG_8618.jpg",
        "/general-oyw/IMG_8619.jpg",
        "/general-oyw/IMG_8622.jpg",
        "/general-oyw/IMG_8624.jpg",
        "/general-oyw/IMG_8633.jpg",
        "/general-oyw/IMG_8634.jpg",
        "/general-oyw/IMG_8639.jpg",
        "/general-oyw/IMG_8641.jpg",
        "/general-oyw/IMG_8642.jpg",
        "/general-oyw/IMG_8643.jpg",
      ],
      theme: "EMOTIONS IN MOTIONS",
    },
    {
      id: "Drama",
      name: "DRAMA EVENT",
      day: 3,
      description:
        "You will see your favourite superhero be transported to another universe where they have to navigate the ups and downs before time runs out.",
      date: "",
      images: [
        "/general-oyw/DSC_1214 (1).jpg",
        "/general-oyw/DSC_1260.jpg",
        "/general-oyw/IMG_7292.jpg",
        "/general-oyw/IMG_7360.jpg",
        "/general-oyw/IMG_7447.jpg",
        "/general-oyw/IMG_7485.jpg",
        "/general-oyw/IMG_7496.jpg",
        "/general-oyw/IMG_7530.jpg",
        "/general-oyw/IMG_7777.jpg",
        "/general-oyw/IMG_7798.jpg",
        "/general-oyw/IMG_8616.jpg",
        "/general-oyw/IMG_8617.jpg",
        "/general-oyw/IMG_8618.jpg",
        "/general-oyw/IMG_8619.jpg",
        "/general-oyw/IMG_8622.jpg",
        "/general-oyw/IMG_8624.jpg",
        "/general-oyw/IMG_8633.jpg",
        "/general-oyw/IMG_8634.jpg",
        "/general-oyw/IMG_8639.jpg",
        "/general-oyw/IMG_8641.jpg",
        "/general-oyw/IMG_8642.jpg",
        "/general-oyw/IMG_8643.jpg",
      ],
      theme: "COSMIC CLASH",
    },
  ],
};

export const CONSTANTS = "OYW 2021 | OLLC . All rights reserved";
export const OLLC_LINK = "https://www.ourladyoflourdeschurchorlem.com/";

export const object_store = {
  "drama-oyw": [
    "https://storage.googleapis.com/drama-oyw/IMG_6781.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6815.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6817.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6831.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6850.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6862.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6872.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6908.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6916.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6928.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6966.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_6982.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_7016.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_7020.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_7065.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_7066.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_7125.JPG",
    "https://storage.googleapis.com/drama-oyw/IMG_7141.JPG",
  ],

  "general-oyw": [
    "https://storage.googleapis.com/general-oyw/DSC_1260.jpg",
    "https://storage.googleapis.com/general-oyw/IMG_7292.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_7360.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_7447.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_7485.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_7496.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_7530.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_7777.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_7798.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8616.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8617.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8618.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8619.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8622.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8624.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8633.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8634.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8639.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8641.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8642.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8643.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8646.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8648.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8650.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8657.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8658.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8662.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8665.JPG",
    "https://storage.googleapis.com/general-oyw/IMG_8667.JPG",
  ],

  "singing-oyw": [
    "https://storage.googleapis.com/singing-oyw/DSC_1327.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1338.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1344.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1385.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1407.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1418.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1435.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1544.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1554.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1561.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1565.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1566.JPG",
    "https://storage.googleapis.com/singing-oyw/DSC_1589.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6460.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6481.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6488.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6516.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6525.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6565.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6571.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6581.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6583.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6608.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6619.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6654.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6674.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6688.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6691.JPG",
    "https://storage.googleapis.com/singing-oyw/IMG_6747.JPG",
  ],

  "react-static": [
    "https://storage.googleapis.com/react-static/logo.ico",
    "https://storage.googleapis.com/react-static/logo_dark_1920.png",
    "https://storage.googleapis.com/react-static/logo_dark_512.png",
    "https://storage.googleapis.com/react-static/logo_light_1920.png",
    "https://storage.googleapis.com/react-static/logo_light_512.png",
  ],
};
