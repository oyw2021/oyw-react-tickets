import { configureStore } from "@reduxjs/toolkit";
import entitiesReducer from "./entities";

export const store = configureStore({
  reducer: {
    entities: entitiesReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;