import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { API_SERVER_URL } from "../utils/constants";
import type { AppDispatch, RootState } from "./configureStore";
import axios from "../utils/httpServices";
import cookie from "react-cookies";

interface Transaction {
  amount: number;
  code: string;
  dance_count: number;
  drama_count: number;
  email: string;
  singing_count: number;
  timestamp: string;
  id: string;
}

interface ScanTransaction {
  amount: number;
  code: string;
  count: number;
  day: number;
  email: string;
  timestamp: string;
  id: string;
}

interface PassDetails {
  phone: string | null;
  email: string;
  singing_count: number;
  dance_count: number;
  drama_redeemed_count: number;
  dance_redeemed_count: number;
  code: string;
  name: string;
  drama_count: number;
  singing_redeemed_count: number;
  short_code: string;
  balance: number;
  pass_number: number;
}

interface PassState {
  loading: boolean;
  message: string;
  checkBalance: boolean;
  balance: number;
  validateModalOpen: boolean;
  passDetails: PassDetails;
  transaction: Transaction[];
  scanTransaction: ScanTransaction[];
  undoModalOpen: boolean;
  undoScanModalOpen: boolean;
  confirmRegisterModalOpen: boolean;
  showQR: boolean;
}

const initialState: PassState = {
  loading: false,
  message: "",
  checkBalance: false,
  balance: 0,
  validateModalOpen: false,
  passDetails: {
    phone: "",
    email: "",
    singing_count: 0,
    dance_count: 0,
    drama_redeemed_count: 0,
    dance_redeemed_count: 0,
    code: "",
    name: "",
    drama_count: 0,
    singing_redeemed_count: 0,
    short_code: "",
    balance: 0,
    pass_number: 0
  },
  transaction: [],
  scanTransaction: [],
  undoModalOpen: false,
  confirmRegisterModalOpen: false,
  undoScanModalOpen: false,
  showQR: true,
};

export const passSlice = createSlice({
  name: "pass",
  initialState,
  reducers: {
    setLoading: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
    },
    setMessage: (state, action: PayloadAction<string>) => {
      state.message = action.payload;
    },
    setCheckBalance: (state, action: PayloadAction<boolean>) => {
      state.checkBalance = action.payload;
    },
    setBalance: (state, action: PayloadAction<number>) => {
      state.balance = action.payload;
    },
    setValidateModal: (state, action: PayloadAction<boolean>) => {
      state.validateModalOpen = action.payload;
    },
    setTransaction: (state, action: PayloadAction<Transaction[]>) => {
      state.transaction = action.payload;
    },
    setScanTransaction: (state, action: PayloadAction<ScanTransaction[]>) => {
      state.scanTransaction = action.payload;
    },
    setUndoModal: (state, action: PayloadAction<boolean>) => {
      state.undoModalOpen = action.payload;
    },
    setPassDetails: (state, action: PayloadAction<PassDetails>) => {
      state.passDetails = action.payload;
    },
    setConfirmRegisterModalOpen: (state, action: PayloadAction<boolean>) => {
      state.confirmRegisterModalOpen = action.payload;
    },
    setUndoScanModalOpen: (state, action: PayloadAction<boolean>) => {
      state.undoScanModalOpen = action.payload;
    },
    setShowQR: (state, action: PayloadAction<boolean>) => {
      state.showQR = action.payload;
    },
  },
});

export const registerPass =
  (body: {
    name: string;
    email?: string;
    phone?: string;
    singing_count: number;
    drama_count: number;
    dance_count: number;
  }) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      console.log(body);
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/passes/register/";
      let response: { [key: string]: any } = await axios.post(url, body, {
        headers: {
          token: token,
        },
      });
      dispatch(setLoading(false));
      if (response.data.result) {
        alert(`Pass for ${body.name} registered successfully.`);
        window.location.href = "/tickets/register";
        dispatch(setMessage(response.data.message));
      } else {
        alert(response.data.message);
      }

      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const scanPass =
  (body: { code?: string; short_code?: string; day: number }) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      
      console.log(body);
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/passes/scan/";
      let response: { [key: string]: any } = await axios.post(url, body, {
        headers: {
          token: token,
        },
      });
      if (response.data.result) {
        dispatch(setCheckBalance(true));
        dispatch(setBalance(response.data.data.balance));
      } else {
        alert(response.data.message);
        dispatch(setShowQR(true));
        dispatch(setBalance(0));
      }
      dispatch(setLoading(false));
      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const acceptScan =
  (body: { code?: string; short_code?: string; day: number; count: number }) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      console.log(body);
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/passes/accept/";
      let response: { [key: string]: any } = await axios.post(url, body, {
        headers: {
          token: token,
        },
      });
      if (response.data.result) {
        alert("Pass scanned successfully!")
        dispatch(setValidateModal(false));
        dispatch(setShowQR(true));
      } else {
        alert("Failed to scan pass!");
      }
      dispatch(setLoading(false));
      console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const getPassDetails =
  (body: { code?: string; short_code?: string }) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      // console.log(body);
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/passes/pass_details/";
      let response: { [key: string]: any } = await axios.post(
        url,
        { code: body.code, short_code: "", day: 0 },
        {
          headers: {
            token: token,
          },
        }
      );
      if (response.data.result) {
        // dispatch(setValidateModal(false));
        dispatch(setPassDetails(response.data.data));
      } else {
      }
      dispatch(setLoading(false));
      // console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const getTransactions =
  () => async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/admin/view_transactions/";
      let response: { [key: string]: any } = await axios.post(
        url,
        {},
        {
          headers: {
            token: token
          },
        }
      );
      if (response.data.result) {
        dispatch(setTransaction(response.data.data.registrations));
        dispatch(setScanTransaction(response.data.data.scans));
      }
      dispatch(setLoading(false));
      // console.log(response);
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const undoTransaction =
  (transaction_id: string) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/admin/undo_register/";
      let response: { [key: string]: any } = await axios.post(
        url,
        { transaction_id },
        {
          headers: {
            token: token,
          },
        }
      );
      console.log(response);
      alert(response.data.message);
      dispatch(setUndoModal(false));
      dispatch(getTransactions());
      if (response.data.result) {
        dispatch(setUndoModal(false));
        dispatch(getTransactions());
      }
      dispatch(setLoading(false));
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const undoScan =
  (transaction_id: string) =>
  async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      dispatch(setLoading(true));
      let token: string = await cookie.load("token");
      let url = API_SERVER_URL + "/admin/undo_scan/";
      let response: { [key: string]: any } = await axios.post(
        url,
        { transaction_id },
        {
          headers: {
            token: token,
          },
        }
      );
      console.log(response);
      if (response.data.result) {
        alert(response.data.message);
        dispatch(setUndoScanModalOpen(false));
        dispatch(getTransactions());
      } else {
        alert(response.data.message);
        dispatch(setUndoScanModalOpen(false));
      }
      dispatch(setLoading(false));
    } catch (error) {
      console.error(error);
    } finally {
    }
  };

export const {
  setLoading,
  setMessage,
  setCheckBalance,
  setBalance,
  setValidateModal,
  setTransaction,
  setUndoModal,
  setPassDetails,
  setConfirmRegisterModalOpen,
  setScanTransaction,
  setUndoScanModalOpen,
  setShowQR,
} = passSlice.actions;

export default passSlice.reducer;
