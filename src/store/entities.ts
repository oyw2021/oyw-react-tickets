import { combineReducers } from "redux";
import homeReducer from './homeSlice';
import adminReducer from "./adminSlice";
import passReducer from "./passSlice";

export default combineReducers({
  home: homeReducer,
  admin: adminReducer,
  pass: passReducer,
});