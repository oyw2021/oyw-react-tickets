import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LandingPage from "./pages/LandingPage";
import EventDetails from "./pages/EventDetails";
import LiveStream from "./pages/LiveStream";
import Login from "./pages/SignUp";
import Scanner from "./pages/Scanner";
import { useAppSelector } from "./store/hooks";
import { Navigate } from "react-router-dom";
import RegisterPass from "./pages/RegisterPass";
import { OLLC_LINK } from "./utils/constants";
import Dashboard from "./pages/Dashboard";
import { AppContainer, CopyrightBanner } from "./components/commonStyles";
import { OutfitText } from "./components/commonText";
import PassDetails from "./pages/PassDetails";
import Passes from "./pages/Passes";
import Admin from "./pages/Admin";

import Transactions from "./pages/Transactions";
function App() {
  const { user } = useAppSelector((state) => state.entities.home);
  return (
    <AppContainer>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/:name" element={<EventDetails />} />
          <Route path="/livestream" element={<LiveStream />} />
          <Route path="/tickets" element={<Dashboard />} />
          <Route path="/tickets/signin" element={<Login />} />
          <Route path="/tickets/passes" element={<Passes />} />
          <Route
            path="/tickets/scanner"
            element={
              user.role ? <Scanner /> : <Navigate to="/tickets/signin" />
            }
          />
          {/* <Route
            path="/tickets/register"
            element={
              [(USER_ROLES.RH, USER_ROLES.RU, USER_ROLES.SA)].includes(
                user.role
              ) ? (
                <RegisterPass />
              ) : (
                <Navigate to="/tickets/signin" />
              )
            }
          /> */}
          <Route path="/tickets/register" element={<RegisterPass />} />
          <Route path="/tickets/pass/:passDetails" element={<PassDetails />} />
          <Route path="/tickets/admin/" element={<Admin />} />
          <Route path="/tickets/transactions/" element={<Transactions />} />
          <Route path="/*" element={<Navigate to="/" />} />
        </Routes>
        <CopyrightBanner>
          <OutfitText
            style={{ fontSize: "10px", color: "white", textAlign: "center" }}
          >
            Copyright &copy; OYW | 
          <a
            href={OLLC_LINK}
            target="_blank"
            rel="noopener"
            style={{ textDecoration: "none" }}
          >  &nbsp;Our Lady of Lourdes Church, Orlem. 2021
          </a> </OutfitText>
        </CopyrightBanner>
      </BrowserRouter>
    </AppContainer>
  );
}

export default App;
